### 李凯强面试题项目
- #### 项目概述
   - 产品名称：图片编辑
- #### 产品大概功能
    
   -# ##### 上传图片
###### URL
> [http://域名/api/image]

###### 支持格式
> JSON

###### HTTP请求方式
> POST

###### 请求参数
>
|参数|必选|类型|说明|
|:-----  |:-------|:-----|-----             |
|image_src    |true    |file|上传图片 |

###### 返回字段
> 
|返回字段|类型  |说明                              |
|:-----    |:------|:-----------------------------   |
|status_code  |int    |状态码  |
###### 成功接口示例
> 
```
{
    "status_code": 2000000
}
```
  
   -# ##### 输出指定规格图片
###### URL
> [http://域名/api/image/nr3jg9n0rk16v8y74yeo527mdxwl4p12/property/w-100/suffix/png]

###### 支持格式
> JSON

###### HTTP请求方式
> get

###### 请求参数
>无

###### 返回字段
>图片
###### 成功接口示例
> 
```
图片
```

- #### 运行环境要求
   - Nginx 1.13+
   - php 7.0+
   - mysql 5.7+
- #### 本地开发环境部署/安装
   **该系统是用php环境laravel5.4，默认的开发环境是[Laravel Homestead](https://d.laravel-china.org/docs/5.5/homestead)**
   **若本地的环境是HomeStead,请按下面步骤进行安装，否则，请参考[Laravel Homestead](https://d.laravel-china.org/docs/5.5/homestead)安装环境**
     - 安装homestead环境；
     - 克隆代码到本地，用git命令行执行 git clone git@gitlab.com:lkq0929/likaiqiang-imageick.git；
     - 安装依赖，命令行执行composer install；
     - 复制.env.example文件到项目根目录下，并改名为.env；
     - 打开.env,配置域名，数据库；
    
- #### 扩展包说明
     扩展包名 | 功能说明|应用场景
      ---|---|---
      intervention/image|图片处理包|支持gd、imageick驱动
      imageick|图片处理扩展|比gd库处理图片速度更快、更方便
      vinkla/hashids|加密和解密图片虚拟码|隐藏图片的真实id
     
    
- #### 命令artisan说明
    - 切换到项目根目录下，执行php artisan 查看命令列表以及功能 ；
