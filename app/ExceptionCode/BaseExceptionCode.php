<?php

namespace App\ExceptionCode;


/**
 * |--------------------------------------------------------------------------
 * | BaseExceptionCode [ 系统状态码 ]
 * |--------------------------------------------------------------------------
 * | @Author Docker
 * |
 * | Class BaseExceptionCode
 * | @package App\ExceptionCode
 */
class BaseExceptionCode
{
    CONST HTTP_OK = 20000;

    CONST HTTP_WRONG = 40000;
    CONST HTTP_NOT_FOUND = 40004;
    CONST MISS_PARAM = 40005;

}