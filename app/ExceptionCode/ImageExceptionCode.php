<?php

namespace App\ExceptionCode;


/**
 * |--------------------------------------------------------------------------
 * | UserExceptionCode [ 图片相关异常错误码 ]
 * |--------------------------------------------------------------------------
 * | @Author Docker
 * |
 * | Class UserExceptionCode
 * | @package App\ExceptionCode
 */
class ImageExceptionCode extends BaseExceptionCode
{
     CONST PARA_INVALID = 10001;
     CONST IMAGE_NOT_FOUND = 10002;
}