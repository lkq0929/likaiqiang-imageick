<?php

namespace App\ExceptionMsg;


/**
 * |--------------------------------------------------------------------------
 * | BaseExceptionMsg [ 系统异常状态提示信息 ]
 * |--------------------------------------------------------------------------
 * | @Author Docker${CARET}
 * |
 * | Class BaseExceptionMsg
 * | @package App\ExceptionMsg
 */
class BaseExceptionMsg
{
    CONST HTTP_OK_MSG = "操作成功";
    CONST HTTP_WRONG_MSG = "操作失败";

    CONST HTTP_NOT_FOUND_MSG = "您请求的资源不存在";
    CONST MISS_PARAM_MSG = "缺少必要的参数";
}