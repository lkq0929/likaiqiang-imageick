<?php

namespace App\ExceptionMsg;


/**
 * |--------------------------------------------------------------------------
 * | UserExceptionMsg [ Comment Here ]
 * |--------------------------------------------------------------------------
 * | @Author Docker${CARET}
 * |
 * | Class UserExceptionMsg
 * | @package App\ExceptionMsg
 */
class ImageExceptionMsg extends BaseExceptionMsg
{
    const PARA_INVALID_MSG = "参数不合法";
    const IMAGE_NOT_MSG = "该图片不存在";
}