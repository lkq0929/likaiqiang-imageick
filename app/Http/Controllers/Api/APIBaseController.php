<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;


/**
 * |--------------------------------------------------------------------------
 * | APIBaseController [ 接口基础类接口 ]
 * |--------------------------------------------------------------------------
 * | @Author Docker<likq@weitianshi.cn>
 * |
 * | Class APIBaseController
 * | @package App\Http\Controllers
 */
class APIBaseController extends Controller
{
    public function __construct()
    {
    }
}