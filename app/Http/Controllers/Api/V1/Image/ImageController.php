<?php

namespace App\Http\Controllers\Api\V1\Image;

use App\Http\Controllers\Api\APIBaseController;
use App\Services\Image\ImageService;
use Illuminate\Http\Request;


/**
 * |--------------------------------------------------------------------------
 * | ImageController [ 图片处理 ]
 * |--------------------------------------------------------------------------
 * | @Author Docker<likq@weitianshi.cn>
 * |
 * | Class ImageController
 * | @package App\Http\Controllers\Api\V1\Image
 */
class ImageController extends APIBaseController
{
    private $image;

    public function __construct(ImageService $image)
    {
        $this->image = $image;
    }

    /**
     *--------------------------------------------------------------------------
     * store [ 存储图片 ]
     *--------------------------------------------------------------------------
     * @Author Docker
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function store(Request $request)
    {
        return $this->image->store($request);
    }

    /**
     *--------------------------------------------------------------------------
     * show [ 图片比例显示 ]
     *--------------------------------------------------------------------------
     * @Author Docker
     *
     * @param $image_id
     * @param $property_name
     * @param $ext
     * @return mixed
     */
    public function show($image_id,$property_name,$ext)
    {
        return $this->image->show($image_id,$property_name,$ext);
    }
}