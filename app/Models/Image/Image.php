<?php

namespace App\Models\Image;

use App\Models\Traits\UploadImageTrait;
use Illuminate\Database\Eloquent\Model;


/**
 * |--------------------------------------------------------------------------
 * | Image [ 图像模型 ]
 * |--------------------------------------------------------------------------
 * | @Author Docker<likq@weitianshi.cn>${CARET}
 * |
 * | Class Image
 * | @package App\Models\Image
 */
class Image  extends Model
{
    use UploadImageTrait;

    protected $primaryKey = "image_id";
    protected $table = "images";
    protected $fillable = [
        'image_name',
        'image_ext',
        'image_src',
        'image_x',
        'image_y'
    ];
}