<?php

namespace App\Models\Traits;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * |--------------------------------------------------------------------------
 * | [ 图片处理 ]
 * |--------------------------------------------------------------------------
 * | @Author Docker
 * |--------------------------------------------------------------------------
 */
trait UploadImageTrait
{
    /**
     *--------------------------------------------------------------------------
     * getAllowedType [ 允许上传的图片类型 ]
     *--------------------------------------------------------------------------
     * @Author Docker
     *
     * @return array
     */
    public function getAllowedType()
    {
        return [
            'image/png',
            'image/jpeg',
            'image/gif',
            'image/bmp'
        ];
    }

    /**
     *--------------------------------------------------------------------------
     * isAllowed [ 检查上传的图片的类型 ]
     *--------------------------------------------------------------------------
     * @Author Docker
     *
     * @param UploadedFile $file
     * @return bool
     */
    public function isAllowed(UploadedFile $file)
    {
        return in_array($file->getClientMimeType(), $this->getAllowedType());
    }


    public function uploadFile(UploadedFile $file)
    {
        $list = [];
        $fileSize = getimagesize($file);
        $list['image_name'] = $file->getClientOriginalName();
        $list['image_ext'] = $file->getClientOriginalExtension();
        $list['image_x'] = $fileSize[0];
        $list['image_y'] = $fileSize[1];
        $dir = '/' . date('Ymd');
        $fileName = str_random(20) . uniqid() . '.' . $list['image_ext'];
        $file->move("./attach" . $dir, $fileName);
        $list['image_src'] = substr("./attach" . $dir . '/' . $fileName, 1);

        return $list;
    }


}