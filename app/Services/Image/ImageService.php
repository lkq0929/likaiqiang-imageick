<?php

namespace App\Services\Image;

use App\ExceptionCode\ImageExceptionCode;
use App\ExceptionMsg\ImageExceptionMsg;
use App\Models\Image\Image;
use App\Services\PublicService;
use Vinkla\Hashids\Facades\Hashids;


/**
 * |--------------------------------------------------------------------------
 * | ImageService [ 图片处理 ]
 * |--------------------------------------------------------------------------
 * | @Author Docker<likq@weitianshi.cn>${CARET}
 * |
 * | Class ImageService
 * | @package App\Services\Image
 */
class ImageService extends PublicService
{
    private $model;

    public function __construct(Image $model)
    {
        $this->model = $model;
    }

    /**
     *--------------------------------------------------------------------------
     * store [ 上传图片 ]
     *--------------------------------------------------------------------------
     * @Author Docker
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function store($request)
    {
        $attributes = [];
        if (!isset($request['image_src'])) {
            return $this->responseError(ImageExceptionCode::MISS_PARAM, ImageExceptionMsg::MISS_PARAM_MSG);
        }
        if ($this->model->isAllowed($request->file('image_src'))) {
            $attributes = $this->model->uploadFile($request->file('image_src'));
        }
        $image = $this->model->create($attributes);
        if (!$image) {
            return $this->responseError(ImageExceptionCode::HTTP_WRONG, ImageExceptionMsg::HTTP_WRONG_MSG);
        }

        return $this->responseSuccess(ImageExceptionCode::HTTP_OK);
    }

    /**
     *--------------------------------------------------------------------------
     * show [ 输出图片 ]
     *--------------------------------------------------------------------------
     * @Author Docker
     *
     * @param $image_id
     * @param $property_name
     * @param $ext
     * @return mixed
     */
    public function show($image_id, $property_name, $ext)
    {
        $allow_type = ['png', 'jpeg', 'gif', 'bmp','jpg','webp'];
        if (strpos($property_name, '-') === false || !in_array($ext,$allow_type)) {
            return $this->responseError(ImageExceptionCode::PARA_INVALID, ImageExceptionMsg::PARA_INVALID_MSG);
        }
        $image_id =  Hashids::decode($image_id);
        $image = $this->model->where('image_id',$image_id)->first();
        if(!$image) {
            return $this->responseError(ImageExceptionCode::IMAGE_NOT_FOUND,ImageExceptionMsg::IMAGE_NOT_MSG);
        }
        $properties = explode('-',$property_name);
        if($properties[0] == "w") {
            $image_x = $properties[1];
            $image_y = $properties[1]*($image->image_y/$image->image_x);
        }
        if($properties[0] == "h") {
            $image_y = $properties[1];
            $image_x = $properties[1]*($image->image_x/$image->image_y);
        }
        $img = \Intervention\Image\Facades\Image::make("http://".env('API_DOMAIN',"imageick.app").$image->image_src)->resize($image_x, $image_y);
        return $img->response($ext);
    }
}