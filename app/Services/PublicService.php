<?php

namespace App\Services;


/**
 * |--------------------------------------------------------------------------
 * | PublicService [ 基础服务类 ]
 * |--------------------------------------------------------------------------
 * | @Author Docker<likq@weitianshi.cn>
 * |
 * | Class PublicService
 * | @package App\Services
 */
class PublicService
{
    /**
     *--------------------------------------------------------------------------
     * responseError [ 接口请求失败 ]
     *--------------------------------------------------------------------------
     * @Author Docker
     *
     * @param int $status_code
     * @param string $error_msg
     * @return mixed
     */
    public function responseError($status_code = 40000, $error_msg = "操作失败")
    {
        return \Response::json([
            'status_code' => $status_code,
            'error_msg' => $error_msg
        ]);
    }

    /**
     *--------------------------------------------------------------------------
     * responseSuccess [ 接口请求成功 ]
     *--------------------------------------------------------------------------
     * @Author Docker
     *
     * @param $status_code
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseSuccess($status_code = 20000, $data = [])
    {
        return empty($data) ? \Response::json([
            'status_code' => $status_code
        ]) : \Response::json([
            'status_code' => $status_code,
            'data' => $data
        ]);
    }
}