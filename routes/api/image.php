<?php
/**
 * |--------------------------------------------------------------------------
 * | [ 图片处理相关路由]
 * |--------------------------------------------------------------------------
 * | @Author Docker
 * |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'image', 'namespace' => "Image"], function () {
    Route::post("/","ImageController@store");
    Route::get("/{image_id}/property/{property}/suffix/{ext}","ImageController@show");
});